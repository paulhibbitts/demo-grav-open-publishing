---
title: Search
published: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
hide_sidebar: false
display_post_summary:
    enabled: false
post_icon: calendar-o
continue_link_as_button: false
hide_git_sync_repo_link: true
icon: search
---

This page uses the open source [TNTSearch plugin](https://github.com/trilbymedia/grav-plugin-tntsearch) and includes the ability for fuzzy (i.e. approximate) searches.
---
title: Blog
body_classes: 'header-image fullwidth'
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
hide_sidebar: false
display_post_summary:
    enabled: false
post_icon: calendar-o
continue_link_as_button: false
hide_git_sync_repo_link: false
sitemap:
    changefreq: monthly
blog_url: blog
feed:
    description: 'Sample Blog Description'
    limit: 10
pagination: true
---

# **Open** Publishing Space
## Create, Publish, Share and Collaborate
